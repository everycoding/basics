var app = angular.module("app",[]);

app.controller("msg",["$scope",function($scope){

}]);

//type - 1, compile with pre, post
// app.directive('message',['$interpolate',function($interpolate){
//     return {
//         compile :  function(tElement,tAttributes){
//             console.log(tAttributes.text   +"- In compile.");
//             tElement.css("border","1px solid #c0c0c0");
//             // this part is link
//             return {
//                 pre: function(scope,iElement,iAttributes){
//                     console.log(iAttributes.text +"- In Pre");
//                 },
//                 post: function(scope,iElement,iAttributes){
//                     console.log(iAttributes.text +"- In Post");
//                     if(iAttributes.text === "3"){
//                         iElement.css("border","1px solid red");
//                     }
//                     //event listener
//                     iElement.on("click",scope.btnClicked);
//                 }
//             }  
//         },
//         controller: function($scope,$element,$attrs){
//             var v = $interpolate($attrs.text)($scope)
//             console.log($attrs.text+"- In controller");
//             console.log(v+"- In controller, after interpolation");
//             $scope.btnClicked = function(){
//                 alert(v);
//             }
//         }
//     }
// }]);

// type -2 , without compile stage
// app.directive('message',['$interpolate',function($interpolate){
//     return {
//         link :{
//             pre: function(scope,iElement,iAttributes,controller){
//                 console.log(iAttributes.text +"- In Pre");
//             },
//             post: function(scope,iElement,iAttributes,controller){
//                 console.log(iAttributes.text +"- In Post");
//                 if(iAttributes.text === "3"){
//                     iElement.css("border","1px solid red");
//                 }
//                 //event listener
//                 iElement.on("click",scope.btnClicked);
//             }
//         },
//         controller: function($scope,$element,$attrs){
//             var v = $interpolate($attrs.text)($scope)
//             console.log($attrs.text+"- In controller");
//             console.log(v+"- In controller, after interpolation");
//             $scope.btnClicked = function(){
//                 alert(v);
//             }
//         }
//     }
// }]);

//type - 3, compile with return(post stage)
// app.directive('message',['$interpolate',function($interpolate){
//     return {
//         compile :  function(tElement,tAttributes){
//             console.log(tAttributes.text   +"- In compile.");
//             tElement.css("border","1px solid #c0c0c0");
//             // this is actually post
//             return function(scope,iElement,iAttributes){
//                     console.log(iAttributes.text +"- In Post");
//                     if(iAttributes.text === "3"){
//                         iElement.css("border","1px solid red");
//                     }
//                     //event listener
//                     iElement.on("click",scope.btnClicked);
//                 }
//         },
//         controller: function($scope,$element,$attrs){
//             var v = $interpolate($attrs.text)($scope)
//             console.log($attrs.text+"- In controller");
//             console.log(v+"- In controller, after interpolation");
//             $scope.btnClicked = function(){
//                 alert(v);
//             }
//         }
//     }
// }]);

//type - 4, only return statement which is post
app.directive('message',['$interpolate',function($interpolate){
    // this is actually post
    return function(scope,iElement,iAttributes){
            console.log(iAttributes.text +"- In Post");
            if(iAttributes.text === "3"){
                iElement.css("border","1px solid red");
            }
    }
}]);