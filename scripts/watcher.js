var app = angular.module("app",[]);
        app.controller("controller",['$scope',function($scope){
            $scope.a = 10;
            $scope.b = 20;
            //by default c is not watched by angular if its not bind in html
            $scope.c = 30;
            $scope.obj = {};
            $scope.elements = [];
            // force watch on $scope.c
            $scope.$watch('c',function(newVal, oldVal){
                newVal!=oldVal ? console.log('updated c to new val:'+newVal): undefined ;
            });    
            $scope.updateC = function(){
                $scope.c = $scope.a*2;
            };
            // watch any of a,b,c modifications
            $scope.$watchGroup(['a','b','c'],function(oldVal,newVal){
                if(oldVal!=newVal){
                    $scope.obj.a = $scope.a;
                    $scope.obj.b = $scope.b;
                    $scope.obj.c = $scope.c;
                }
            });
            // reference watch, works only if reference in modified
            $scope.$watch('obj',function(oldVal,newVal){
                if(oldVal!=newVal){
                   console.log("obj ref has been modified:"+ $scope.obj);
                }
            });
            // equality watch, works for any modification
            $scope.$watch('obj',function(oldVal,newVal){
                if(oldVal!=newVal){
                   console.log("obj has been modified:"+ $scope.obj);
                }
            },true);
            //works only any new ref, new obj added or removed
            $scope.$watchCollection('elements',function(oldVal,newVal){
                if(oldVal!=newVal){
                   console.log("elements has been modified:"+ $scope.elements);
                }
            });
            //equality watcher on collection, wroks for any change in collection or its element obj
            $scope.$watch('elements',function(oldVal,newVal){
                if(oldVal!=newVal){
                   console.log("any change in elements list");
                }
            },true);

            $scope.addToList = function(){
                $scope.elements.push($scope.obj);
                $scope.obj = {};
            };

            $scope.emptyList = function(){
                $scope.elements = [];
            }
        }]);