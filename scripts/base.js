var app_module=angular.module("app",[]); // new module "app" has been created, [] means new module with no dependency

app_module.controller("my-controller",function(){
    var ctrl = this;
    this.name = "";
    this.message = "";

    ctrl.hello = function(){
        this.message = this.name != "" ? "Hello "+ this.name : "" ;
    }
});