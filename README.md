# README #

This README would normally document whatever steps are necessary to get your application up and running.

# scripts
<!-- j-query -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- angular scripts(local) -->
<script src="../angular-1.7.8/angular.js"></script> 
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

# create server at localhost
>npm install -g http-server
>http-server (to start http server from inside htmlpages to access them in server)

### Instructions ###

* controller (http://bit.ly/2YeBBOo)
* $scope (http://bit.ly/2MhbS0t)
* compile and link (http://bit.ly/2OLTQGC)
* watcher (http://bit.ly/2Yp2fE4)


### Courtesy ###

* Tech CBT (youtube channel)
* Stack Overflow
* Google